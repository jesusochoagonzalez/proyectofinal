const theme = {
    fondo: '#F9F9F9',
    colorPrimario: '#fffff',
    colorPrimarioHover: '#948e90',
    colorSecundario: '#fffff',
    verde: '#43A854',
    rojo: '#E34747'
}

export default theme;