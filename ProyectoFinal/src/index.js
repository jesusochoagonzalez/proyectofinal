import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// Librerias
import WebFont from 'webfontloader';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {Helmet} from "react-helmet";

// Contextos
import { AuthProvider } from './contextos/AuthContext';

// Elementos
import Contenedor from './elementos/Contenedor';

// Componentes
import InicioSesion from './componentes/InicioSesion';
import RutaPrivada from './componentes/RutaPrivada';

// Img
import favicon from './img/logo.png';

WebFont.load({
  google: {
    // Work+Sans:wght@400;500;700
    families: ['Work Sans:400,500,700', 'sans-serif']
  }
});

const Index = () => {
  return (
    <>
      <Helmet>
        <link rel="shortcut icon" href={favicon} type="image/x-icon"/>
      </Helmet>

      <AuthProvider>
          <BrowserRouter>
            <Contenedor>
              <Switch>
                <Route path="/iniciar-sesion" component={InicioSesion}/>
                <RutaPrivada path="/"> <App /> </RutaPrivada>
                <Route path="*"><RutaPrivada path="/"> <App /> </RutaPrivada></Route>
              </Switch>
            </Contenedor>
          </BrowserRouter>
      </AuthProvider>
      
    </>
  );
}

ReactDOM.render(<Index/>,document.getElementById('root'));
