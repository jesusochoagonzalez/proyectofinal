import React, { useState, useEffect, useRef } from 'react';

// Elementos
import { Formulario, Input, ContenedorBoton } from '../elementos/ElementosDeFormulario';
import Boton from '../elementos/Boton';
import Alerta from '../elementos/Alerta';
import {SelectorImg, Selector, SelectorLabel, SelectorContenedor, ContenedorUrl} from '../elementos/SelectorImagen';
import BotonCerrarSesion from '../elementos/BotonCerrarSesion';
// Img
import { ReactComponent as IconoPlus } from '../img/plus.svg';
import SinImagen from '../img/camara.jpg';

// firebase
import agregarProducto from '../firebase/agregarProducto';

const FormularioGasto = () => {
    const [inputArticulo, setInputArticulo] = useState('');
    const [inputDescripcion, setInputDescripcion] = useState('');
    const [inputPrecio, setInputPrecio] = useState(0);
    // Image Chooser
    const [inputFile, setInputFile] = useState('');
    const [imagen, setImagen] = useState(SinImagen);
    const ref = useRef();
    // Alerta
    const [estadoAlerta, setEstadoAlerta] = useState(false);
    const [alerta, setAlerta] = useState({});

    // Colocar imagen seleccionada en una etiqueta img
    useEffect(() => {
        if(inputFile.length > 0) {
            setImagen(URL.createObjectURL(inputFile[0]));
        }
    },[inputFile]);

    const handleChange = (e) => {
        e.preventDefault();

        if( e.target.name === 'articulo' ) {
            setInputArticulo(e.target.value);
        }
        else if( e.target.name === 'descripcion' ){
            setInputDescripcion(e.target.value);
        }
        else if( e.target.name === 'precio' ){
            setInputPrecio(e.target.value);
        }
        else if(e.target.name === 'file') {
            setInputFile(e.target.files);
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        // Comprobamos que los campos no esten vacios
        if(inputDescripcion !== '' && inputArticulo !== '' && inputFile && inputPrecio !== 0) {
            // Agregar 
            agregarProducto({
                articulo: inputArticulo,
                descripcion: inputDescripcion,
                precio: inputPrecio,
                imagen: inputFile[0]
            })
            .then(() => {
                // Limpiar inputs
                limpiarInputs();
                setEstadoAlerta(true);
                setAlerta({
                    tipo: 'exito',
                    mensaje: 'Agregado correctamente.'
                });
            })
            .catch((error) => {
                setEstadoAlerta(true);
                if(error.message === "Existe"){
                    setAlerta({
                        tipo: 'error',
                        mensaje: 'Ya existe un registro con ese articulo.'
                    });
                } else {
                    setAlerta({
                        tipo: 'error',
                        mensaje: 'Hubo un problema al intentar agregar.'
                    });
                }
            });  
        }
        else {
            setEstadoAlerta(true);
            setAlerta({
                tipo: 'error',
                mensaje: 'Por favor rellene todos los campos.'
            });
        }
    }

    const limpiarInputs = () => {
        setInputDescripcion('');
        setInputArticulo('');
        setInputPrecio(0);
        setInputFile('');
        setImagen(SinImagen);
        ref.current.value = "";
    }

    return (
        <Formulario onSubmit={handleSubmit}>
            <Input 
                type="text" 
                name="articulo"
                id="articulo"
                placeholder="Articulo"
                value={inputArticulo}
                onChange={handleChange}
            />
            <Input
                type="text"
                name="descripcion"
                id="descripcion"
                placeholder="Descripción"
                value={inputDescripcion}
                onChange={handleChange}
            />
            <Input
                type="number"
                name="precio"
                id="precio"
                placeholder="Precio"
                value={inputPrecio}
                onChange={handleChange}
            />
            <SelectorContenedor>
                <SelectorImg src={imagen} />
                <Selector 
                    type="file" 
                    id="file" 
                    name="file"
                    accept="image/*"
                    multiple={false}
                    onChange={handleChange}
                    ref={ref}
                />
                <ContenedorUrl>{imagen === SinImagen ? "Dirección:" : imagen}</ContenedorUrl>
                <SelectorLabel htmlFor="file" >Seleccionar Imagen</SelectorLabel>
            </SelectorContenedor>
            <ContenedorBoton>
                <BotonCerrarSesion />
                <Boton as="button" type="button" primario onClick={limpiarInputs}>Limpiar</Boton>
                <Boton as="button" type="submit" primario conIcono>
                    Agregar
                </Boton>
            </ContenedorBoton>

            <Alerta 
                tipo={alerta.tipo}
                mensaje={alerta.mensaje}
                estadoAlerta={estadoAlerta}
                cambiarEstadoAlerta={setEstadoAlerta}
            />
        </Formulario>
    );
}
 
export default FormularioGasto;