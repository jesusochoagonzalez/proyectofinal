import React from 'react';
import { useHistory } from 'react-router-dom';
import { auth } from '../firebase/firebaseConfig';

// Elementos
import Boton from './Boton';


const BotonCerrarSesion = () => {
    const history = useHistory();

    const cerrarSesion = async () => {
        try{
            await auth.signOut();
            history.push('/iniciar-sesion');
        }
        catch(error) {
            console.log(error);
        }
    }

    return (
        <Boton iconoGrande as="button" onClick={cerrarSesion} primario>
            Salir
        </Boton>
    );
}
 
export default BotonCerrarSesion;
