import firebase from 'firebase/app';
import 'firebase/auth';
import "firebase/database";
import "firebase/storage";

// Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCZbVD9KFrJyndOkh2S5YuiB2aYyi0LeVo",
    authDomain: "agendaproductos-3550e.firebaseapp.com",
    databaseURL: "https://agendaproductos-3550e-default-rtdb.firebaseio.com",
    projectId: "agendaproductos-3550e",
    storageBucket: "agendaproductos-3550e.appspot.com",
    messagingSenderId: "8986067133",
    appId: "1:8986067133:web:b9598be7b7aa95c88c37d4"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const db = firebase.database();
const storage = firebase.storage();
const auth = firebase.auth();

export {db, auth, storage};