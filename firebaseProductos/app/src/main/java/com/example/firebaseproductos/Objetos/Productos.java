package com.example.firebaseproductos.Objetos;

import java.io.Serializable;

public class Productos implements Serializable {
    private String _ID;
    private String articulo;
    private String descripcion;
    private String imagen;
    private String precio;

    public Productos(){

    }
    public Productos(String articulo, String descripcion, String imagen, String precio){
        this.setArticulo(articulo);
        this.setDescripcion(descripcion);
        this.setImagen(imagen);
        this.setPrecio(precio);
    }

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
