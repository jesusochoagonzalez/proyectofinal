package com.example.firebaseproductos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firebaseproductos.Objetos.Productos;
import com.example.firebaseproductos.Objetos.ReferenciasFirebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductoAltaActivity extends AppCompatActivity implements View.OnClickListener {
    String URLORG;
    EditText txtArticulo, txtDescripcion, txtPrecio;
    Button btnImagen, btnRegresar, btnBorrar, btnGuardar;
    private ImageView imgProducto;
    private Uri selectedImgUri;
    private FirebaseDatabase database;
    private DatabaseReference referencia;
    private Productos savedContacto;
    private StorageReference mStorageRef;
    private StorageTask<UploadTask.TaskSnapshot> mUploadTask;
    private String thumb_download_url , id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto_alta);

        this.initComponentes();
        this.setEvents();

        Bundle oBundle = getIntent().getExtras();
        if (oBundle != null) {
            this.initBundle(oBundle);
        }
    }

    private void initComponentes() {
        this.txtArticulo = findViewById(R.id.txtArticulo);
        this.txtDescripcion = findViewById(R.id.txtDescripcion);
        this.txtPrecio = findViewById(R.id.txtPrecio);
        this.btnImagen = findViewById(R.id.btnCargar);
        this.btnRegresar = findViewById(R.id.btnRegresar);
        this.btnBorrar = findViewById(R.id.btnEliminar);
        this.btnGuardar = findViewById(R.id.btnGuardar);
        this.imgProducto = findViewById(R.id.imgPelicula);

        this.database = FirebaseDatabase.getInstance();
        this.mStorageRef = FirebaseStorage.getInstance().getReference();
        this.referencia = this.database.getReferenceFromUrl(
                ReferenciasFirebase.URL_DATABASE +
                        ReferenciasFirebase.DATABASE_NAME + "/" + ReferenciasFirebase.TABLE_NAME
        );

        this.savedContacto = null;
        this.selectedImgUri = null;
    }

    private void initBundle(Bundle oBundle) {
        Productos articulo = (Productos) oBundle.getSerializable("producto");
        this.savedContacto = articulo;
        this.id = articulo.get_ID();
        this.txtArticulo.setText(articulo.getArticulo());
        this.txtDescripcion.setText(articulo.getDescripcion());
        this.txtPrecio.setText(articulo.getPrecio());
        URLORG = articulo.getImagen();
        Picasso.get().load(Uri.parse(articulo.getImagen())).into(imgProducto);
    }

    public void setEvents() {
        this.btnImagen.setOnClickListener(this);
        this.btnRegresar.setOnClickListener(this);
        this.btnBorrar.setOnClickListener(this);
        this.btnGuardar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCargar:
                openFileChooser();
                break;
            case R.id.btnRegresar:
                setResult(Activity.RESULT_OK);
                finish();
                break;
            case R.id.btnEliminar:
                borrarArticulo(id);
                setResult(Activity.RESULT_OK);
                finish();
                break;
            case R.id.btnGuardar:
                Productos nProductos = new Productos();
                nProductos.setArticulo(txtArticulo.getText().toString());
                nProductos.setDescripcion(txtDescripcion.getText().toString());
                nProductos.setPrecio(txtPrecio.getText().toString());
                actualizarArticulo(id,nProductos);
                break;
        }
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),0);
    }

    public void actualizarArticulo(String id, Productos producto) {
        if (selectedImgUri != null) {

            StorageReference filePath = mStorageRef.child("pics").child(selectedImgUri.getLastPathSegment());
            mUploadTask = filePath.putFile(selectedImgUri).addOnSuccessListener(taskSnapshot ->
                    mUploadTask.continueWithTask(task -> {
                        if (!task.isSuccessful()) throw task.getException();

                        // Continue with the task to get the download URL
                        return filePath.getDownloadUrl();
                    }).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            // Guardar url de la imagen
                            thumb_download_url = task.getResult().toString();
                            producto.setImagen(thumb_download_url);

                            // Actualizar en bd
                            this.actualizarProductos(id,producto);

                            setResult(Activity.RESULT_OK);
                            Toast.makeText(getApplicationContext(),"Producto actualizado con exito", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), ListaActivity.class); intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); startActivity(intent);

                        }
                    })
            );
        } else {
            producto.setImagen(URLORG);

            // Actualizar en bd
            this.actualizarProductos(id, producto);

            setResult(Activity.RESULT_OK);
            Toast.makeText(getApplicationContext(),"Producto actualizado con exito", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), ListaActivity.class); intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); startActivity(intent);
        }
    }

    public void borrarArticulo(String id) {
        borrarProducto(id);
        Toast.makeText(getApplicationContext(),"Producto eliminado con exito", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), ListaActivity.class); intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            selectedImgUri = data.getData();
            imgProducto.setImageURI(Uri.parse(selectedImgUri.toString()));
        }
    }
    public void actualizarProductos(String id, Productos p) {
        //actualizar un objeto al nodo referencia
        p.set_ID(id);
        referencia.child(String.valueOf(id)).setValue(p);
    }
    public void borrarProducto(String childIndex){
        referencia.child(String.valueOf(childIndex)).removeValue();
    }

}